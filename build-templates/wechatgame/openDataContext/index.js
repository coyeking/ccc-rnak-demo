import {rankStyle} from './render/style'
import {setRankTemplate} from './render/template'
import Layout from './engine'
import {friendRankData, getFriendRankData } from './render/dataRank'

let __env = GameGlobal.wx || GameGlobal.tt || GameGlobal.swan;
let sharedCanvas  = __env.getSharedCanvas();
let sharedContext = sharedCanvas.getContext('2d');
function draw() {
    Layout.clear();
    Layout.init(setRankTemplate(friendRankData), rankStyle);
    Layout.layout(sharedContext);
}

function updateViewPort(data) { 
    Layout.updateViewPort({
        x: data.x,
        y: data.y,
        width: data.width,
        height: data.height,
    });
}

__env.onMessage(data => {
    if ( data.type === 'engine' && data.event === 'viewport' ) {
        updateViewPort(data);
    }else if (data.value === 'rankData'){
        getFriendRankData("theMess", draw)
    }
});
