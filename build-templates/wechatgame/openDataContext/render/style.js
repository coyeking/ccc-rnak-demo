export let rankStyle = {
    container: {
        width: '100%',
        height: '100%',
        alignItems: 'center'
    },


    rankList: {
        width: '100%',
        height: '90%',
        backgroundColor: 'rgba(255,255,255,0)',
    },

    list: {
        width: '100%',
        height: '88%',
    },

    listTips: {
        width: '100%',
        height: '12%',
        lineHeight: 90,
        textAlign: 'center',
        fontSize: 25,
        color: 'rgba(0,0,0,0.5)',
        backgroundColor: 'rgba(255, 255, 255, 0)',
        borderRadius: 10,
        borderWidth: 1,
    },

    listItem: {
        backgroundColor: 'rgba(255, 255, 255, 0)',
        width: '100%',
        height: '10%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 30,
    },

    listItemUserData: {
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },

    listItemScore: {
        height: 100,
        fontSize: 33,
        fontWeight: 'bold',
        paddingRight : 200,
        lineHeight: 100,
        textAlign: 'center',
    },

    listItemNum: {
        width: 100,
        height: 80,
        fontSize: 30,
        fontWeight: 'bold',
        color: '#452E27',
        lineHeight: 100,
        textAlign: 'center',
    },

    listHeadImg: {
        borderRadius: 6,
        width: 70,
        height: 70,
    },

    listItemName:{
        width: 210,
        height: 100,
        fontSize: 30,
        lineHeight: 100,
        marginLeft: 30,
    },
}
