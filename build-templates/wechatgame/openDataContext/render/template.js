export function setRankTemplate(it) {
	var out =
		'<view class="container" id="main">  <view class="rankList"> <scrollview class="list"> ';
	var arr1 = it.data;
	if (arr1) {
		var item, index = -1,
			l1 = arr1.length - 1;
		while (index < l1) {
			item = arr1[index += 1];
			out += ' ';
			if (index % 2 === 1) {
				out += ' <view class="listItem"> ';
			}
			out += ' ';
			if (index % 2 === 0) {
				out += ' <view class="listItem"> ';
			}
			out += ' <view id="listItemUserData"> <text class="listItemNum" value="' + (index + 1) +
				'"></text> <image class="listHeadImg" src="' + (item.avatarUrl) +
				'"></image> <text class="listItemName" value="' + (item.nickname) +
                '"></text> </view> <text class="listItemScore" value="通关 ' + (item.KVDataList[0].value) + ' 局"></text> </view> ';
		}
	}
	out += ' </scrollview> <text class="listTips" value="' + (it.data.length) + ' 位好友参加了残局挑战"></text> </view></view>';
	return out;
}