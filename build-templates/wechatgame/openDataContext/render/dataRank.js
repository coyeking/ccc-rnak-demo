
export let friendRankData = {
    data: [
        /*
        {
            rankScore: 0,
            avatarUrl: '',
            nickname: '',
        },
        */
    ],
    itemBg: "",
};
/**
 * 获取好友排行榜列表
 */
export function getFriendRankData(key, callback) {
    wx.getFriendCloudStorage({
        keyList: [key],
        success: res => {
            // console.log("getFriendData success--------", res);
            friendRankData.data = res.data;
            friendRankData.data.sort((a, b) => b.KVDataList[0].value - a.KVDataList[0].value);
            callback && callback();
        },
        fail: res => {
            // console.log("getFriendData fail--------", res);
            callback && callback(res.data);
        },
    });
}