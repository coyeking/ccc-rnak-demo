import { _decorator, Component, Node, Prefab, instantiate } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('main')
export class main extends Component {
    @property({
        type: Prefab,
        displayName:'排行榜预制体'
    })
    private rankPrefab: Prefab = null;

    start() {
        window['wx'].setUserCloudStorage({
            KVDataList: [{"key":'theMess', "value": '19'}]
        }).then(res=>{
        }).catch(err=>{
        });
    }

    update(deltaTime: number) {
        
    }

    onclick(event,customEventData){
        let node = instantiate(this.rankPrefab);
        this.node.addChild(node);
    }
}

