本文以一个象棋游戏中，通过关卡排行榜为例；


# 说明
- 排行榜使用了小游戏**开放数据域**，适用于用户托管数据进行写数据操作；
- **demo下载地址** : [点击去下载](https://gitee.com/coyeking/ccc-rnak-demo)
# 原理
 - 将用户已通过关卡数（整型），按标准格式要求数据托管到微信，直接就可获取排行榜数组数据，最后处理界面即可；
 - 在Cocos Creator 3.0 中废弃了之前的 Canvas Renderer 模块（主域+子域模式），使用**Canvas**（XML + CSS设计）代替；
 - 排行榜（开放数据域）只能在离屏画布 **sharedCanvas** 上渲染，所以在项目中，需要有一个节点作为渲染开放数据域的容器，并在该节点上添加 **SubContextView** 组件，该组件会将 sharedCanvas 渲染到容器节点上。
# 步骤如下：
###  1. 排行榜配置
 在`微信小游戏后台 -> 设置 -> 游戏设置 -> 排行榜设置` 中添加配置，此处残局，标识为`theMess`,提交并需要通过审核；
 ![微信小游戏后台](https://img-blog.csdnimg.cn/977225dab89e4ffabe69ec4922f79581.png#pic_center)
###  2. Cocos Creator中操作
- 项目分为初始场景与点击加载排行榜页面；
- 在**初始场景中**对用户托管数据进行写数据操作，[wx.setUserCloudStorage文档](https://developers.weixin.qq.com/minigame/dev/api/open-api/data/wx.setUserCloudStorage.html)，实际项目中按实际需求托管，代码如下：

```typescript
/**
 * 托管数据
 * 需注意KVDataList数组中，value为string
 * return  Promise
 */
window['wx'].setUserCloudStorage({
    KVDataList: [{"key":'theMess', "value": '19'}]
}).then(res=>{
    console.log(res);
}).catch(err=>{
    console.log(err);
});
```
- 在**排行榜页面**预制体中，创建节点`Node-subContextView`，作为作为渲染开放数据域的容器，节点上添加`SubContextView`组件，width与height尽量与节点相同，css中将画布渲染父节点为此处填的，这边方便做背景一些；
![SubContextView](https://img-blog.csdnimg.cn/57dde862ae1741bb9141571f2533e52e.png#pic_center)
- 在**排行榜页面**脚本中，直接向开放数据域抛出消息，命令拉取数据进行渲染，[（postMessage）](https://developers.weixin.qq.com/minigame/dev/api/open-api/context/OpenDataContext.postMessage.html)，代码如下：
```typescript
/*
* 向开放数据域发送消息
*/
window['wx'].getOpenDataContext().postMessage({
    value: 'rankData',
});
```
- 构建发布
勾选  `生成开放数据域工程模版`，然后点击 `构建`；build下会生成一个`openDataContext`文件夹，**此文件夹不能改名**
```
openDataContext
├── engine.js                         // Canvas 引擎源码，DOM渲染（不用管）
├── index.js                           // 开放数据域入口
├── render                           
    ├── mobilediy
           ├── style.js                // css样式
           ├── template.js         // template
           ├── dataRank.js       // 数据拉取方法
```
- 在index.js中收到来自游戏层消息`rankData`，获取列表信息，渲染界面，`theMess`为微信后台添加的排行榜标识；

```javascript
__env.onMessage(data => {
    if ( data.type === 'engine' && data.event === 'viewport' ) {
        updateViewPort(data);
    }else if (data.value === 'rankData'){
        getFriendRankData("theMess", draw)
    }
});
```
- template.js中使用 **doT 模版引擎** 生成 XML 文本，为了方便UI调试，可以在线编辑预览效果: [Playground](https://wechat-miniprogram.github.io/minigame-canvas-engine/playground.html)
- dataRank.js获取好友排行榜列表：

```javascript
/**
 * 获取好友排行榜列表
 */
export function getFriendRankData(key, callback) {
    wx.getFriendCloudStorage({
        keyList: [key],
        success: res => {
            // console.log("getFriendData success--------", res);
            friendRankData.data = res.data;
            friendRankData.data.sort((a, b) => b.KVDataList[0].value - a.KVDataList[0].value);
            callback && callback();
        },
        fail: res => {
            // console.log("getFriendData fail--------", res);
            callback && callback(res.data);
        },
    });
}
```
## 小程序体验
![在这里插入图片描述](https://img-blog.csdnimg.cn/8762da2570ad4553b7a83d91dccd9791.jpeg#pic_center)

## 最终效果
![在这里插入图片描述](https://img-blog.csdnimg.cn/05d7180bf3cc493f8aa10ce7bc7fde0e.png#pic_center)